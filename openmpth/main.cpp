#include <iostream>
#include "omp.h"
#include <ctime>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <omp.h>
using namespace std;

bool IsPointInCircle(double R, double x, double y);

int main()
{
	int k = 4;
#pragma omp parallel
	{
		for (int i = 0; i< k; i++)
			cout << "Hello world!" << endl;
	}
	int Npoints = (int)1E3;
	int Nincircle = 0;
	srand(time(0));
	omp_set_num_threads(k);
#pragma omp parallel
	{
		for (int i = 0; i < Npoints; ++i)
		{
			if
				(
				IsPointInCircle
				(
				1.0,
				(rand() % (4 * Npoints) - 2 * Npoints) / (2.0*Npoints),
				(rand() % (4 * Npoints) - 2 * Npoints) / (2.0*Npoints)
				)
				)
				Nincircle++;
		}
	}
	cout << "Calc  Value of PI : " << setprecision(5) << Nincircle*4.0 / Npoints << endl;
	cout << "Check Value of PI : " << setprecision(5) << asin(1.0)*2.0 << endl;
	cin.get();
	return 0;
}
bool IsPointInCircle(double R, double x, double y)
{
	return ((x*x + y*y) < R*R);
}
